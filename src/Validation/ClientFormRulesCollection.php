<?php

namespace EquityRelease\ClientCriteria\Validation;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

/**
 * Class ClientFormRulesCollection
 * @package EquityRelease\ClientCriteria\Validation
 */
abstract class ClientFormRulesCollection
{
    /**
     * Current request
     *
     * @var Request $request
     */
    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * @inheritdoc
     */
    public function configureRules(): array
    {
        $rules = array_merge(
            $this->configureLoanDetailsRules(),
            $this->configureYourDetailsRules(),
            $this->configureCreditStatusRules(),
            $this->configurePropertyDetailsRules(),
            $this->configureCustomRules(),
            (in_array($this->request->get('loanDetails_equityReleasePlan'), ['retirementMortgage', 'rioMortgage']))
                ? $this->configureIncomeStatusRules()
                : []
        );

        return $rules;
    }

    /**
     * Configure rules for the 'load details' section
     *
     * @return array
     */
    protected function configureLoanDetailsRules(): array
    {
        $rules = [
            'loanDetails_equityRelease' => 'required|array',
            'loanDetails_equityRelease.*' => [
                Rule::in([
                    'debtConsolidation',
                    'homeImprovements',
                    'repayMortgage',
                    'newSecondHome',
                    'supplementRetirementIncome',
                    'newCar',
                    'gifting',
                    'holiday',
                    'inheritanceTax',
                    'carePlans',
                    'emergencyFunds',
                    'other',
                ]),
            ],
            'loanDetails_whatWouldYouLikeToRelease' => 'required|in:cashLumpSum,monthlyIncome',
            'loanDetails_howMatchRelease' => [
                'required',
                Rule::in([
                    'maximum',
                    'specificAmount',
                ]),
            ],
            'loanDetails_equityReleasePlan' => [
                'required',
                Rule::in([
                    'lifetimeMortgage',
                    'retirementMortgage',
                    'homeReversion',
                    'rioMortgage',
                ]),
            ],
            'loanDetails_mortgagePurpose' => [
                'required_if:loanDetails_equityReleasePlan,retirementMortgage',
                'required_if:loanDetails_equityReleasePlan,rioMortgage',
                Rule::in([
                    'housePurchase',
                    'remortgage',
                ]),
            ],
            'loanDetails_mortgageTermRequired' => [
                'required_if:loanDetails_equityReleasePlan,retirementMortgage',
                Rule::in([
                    'fixedTerm',
                    'lifetime',
                ]),
            ],
            'loanDetails_term' => 'required_if:loanDetails_mortgageTermRequired,fixedTerm|integer|between:1,45',
        ];

        if ($this->request->has('loanDetails_whatWouldYouLikeToRelease')
            && $this->request->get('loanDetails_whatWouldYouLikeToRelease') === 'monthlyIncome'
        ) {
            if (!in_array('supplementRetirementIncome', $this->request->get('loanDetails_equityRelease'))) {
                $rules['loanDetails_equityRelease'] .= '|size:' . (count($this->request->get('loanDetails_equityRelease')) + 1);
            }

            if ($this->request->has('loanDetails_howMatchRelease') && $this->request->get('loanDetails_howMatchRelease') === 'specificAmount') {
                $rules['loanDetails_howMatchReleaseSum'] = 'required|numeric|min:200';
            }

            $rules['loanDetails_overHowManyYearsIsIncomeRequired'] = [
                'required',
                'array',
                Rule::in([10, 15, 20, 25]),
            ];

            $rules['loanDetails_equityReleasePlan'] = [
                'required',
                Rule::in([
                    'lifetimeMortgage',
                ]),
            ];
        }

        if ($this->request->has('loanDetails_whatWouldYouLikeToRelease')
            && $this->request->get('loanDetails_whatWouldYouLikeToRelease') === 'cashLumpSum'
        ) {
            $rules['loanDetails_howMatchReleaseSum'] = 'required_if:loanDetails_howMatchRelease,specificAmount|numeric|min:10000';
        }

        if ($this->request->get('loanDetails_equityReleasePlan') === 'homeReversion') {
            $rules['loanDetails_howMatchReleaseSum'] = 'numeric|min:10000';
            $rules['loanDetails_lumpSum'] = 'required_if:loanDetails_sizeOfRelease,lumpSum|numeric|min:10000';
            $rules['loanDetails_propertySell'] = 'required_if:loanDetails_sizeOfRelease,sellProperty|integer|max:100';

            $rules['loanDetails_sizeOfRelease'] = [
                'required',
                Rule::in([
                    'maximumLumpSum',
                    'lumpSum',
                    'sellProperty',
                ]),
            ];
        }

        if (in_array($this->request->get('loanDetails_equityReleasePlan'), ['retirementMortgage'])) {
            $rules['loanDetails_repaymentPreference'] = 'required|in:capitalAndInterest,interestOnly';
        }

        return $rules;
    }

    /**
     * Configure rules for the 'your details' section
     *
     * @return array
     */
    protected function configureYourDetailsRules(): array
    {
        $rules = [
            'yourDetails_applicationType' => [
                'required',
                Rule::in([
                    'single',
                    'joint',
                ]),
            ],
            'yourDetails_title' => [
                'required',
                Rule::in([
                    'mr',
                    'mrs',
                    'miss',
                    'ms',
                    'dr',
                    'prof',
                    'rev',
                    'dame',
                    'lady',
                    'sir',
                    'lord',
                    'mx',
                ]),
            ],
            'yourDetails_firstName' => 'required',
            'yourDetails_surname' => 'required',
            'yourDetails_telephone' => 'required',
            'yourDetails_email' => 'required|email',
            'yourDetails_birthdayDay' => 'required|integer|min:1|max:31',
            'yourDetails_birthdayMonth' => 'required|integer|min:1|max:12',
            'yourDetails_birthdayYear' => 'required|integer|min:1920',
            // Partner birthday
            'yourDetails_birthdayPartnerDay' => 'required_if:yourDetails_applicationType,joint|nullable|integer|min:1|max:31',
            'yourDetails_birthdayPartnerMonth' => 'required_if:yourDetails_applicationType,joint|nullable|integer|min:1|max:12',
            'yourDetails_birthdayPartnerYear' => 'required_if:yourDetails_applicationType,joint|nullable|integer|min:1920',
            'yourDetails_gender' => [
                'required_if:loanDetails_equityReleasePlan,homeReversion',
                Rule::in([
                    'male',
                    'female',
                ]),
            ],
            'yourDetails_maritalStatus' => [
                'required',
                Rule::in([
                    'single',
                    'married',
                    'livingWithPartner',
                    'widowed',
                    'separated',
                    'divorced',
                    'civilPartnership',
                ]),
            ],
            'yourDetails_healthConditions' => [
                'required',
                'boolean',
            ],
            'yourDetails_healthConditionsThatApplyToYou' => 'array',
            'yourDetails_healthConditionsThatApplyToYou.*' => [
                Rule::in([
                    'smokingMoreThan10CigarettesADay',
                    'cancer',
                    'ms',
                    'highBloodPressure',
                    'diabetes',
                    'peripheralVascularDisease',
                    'hepatitisC',
                    'parkinsons',
                    'motorNeuronDisease',
                    'heartAttack',
                    'stroke',
                    'miniStroke',
                    'hiv',
                    'dementia',
                    'kidneyFailure',
                    'organTransplant',
                    'cirrhosisOfLiver',
                    'heartValveReplacement',
                    'angina',
                    'earlyRetirement',
                ]),
            ],
            'yourDetails_postcode' => 'required',
            'yourDetails_houseNumberOrName' => 'required',
            'yourDetails_addressLine1' => 'required',
            'yourDetails_cityTown' => 'required',
            'yourDetails_county' => 'required',
            'yourDetails_country' => [
                'required',
                Rule::in([
                    'england',
                    'wales',
                    'mainlandScotland',
                    'nIreland',
                    'scottishIsles',
                    'isleOfMan',
                    'jersey',
                    'guernsey',
                    'channelIslands',
                ]),
            ],
            'yourDetails_addressSearchType' => 'in:postcode,manual',
            'yourDetails_propertyValue' => 'required|numeric',
            'yourDetails_outstandingMortgageOnThisProperty' => 'required|boolean',
            'yourDetails_mortgageBalanceOutstanding' => 'required_if:yourDetails_outstandingMortgageOnThisProperty,1|numeric',
        ];

        if ($this->request->get('loanDetails_equityReleasePlan') === 'lifetimeMortgage') {
            $rules['yourDetails_doYouOwnThisProperty'] = 'required|boolean';
            $rules['yourDetails_propertyValue'] = 'required_if:yourDetails_doYouOwnThisProperty,1|numeric';
            $rules['yourDetails_outstandingMortgageOnThisProperty'] = 'required_if:yourDetails_doYouOwnThisProperty,1|boolean';
        }

        if ($this->request->get('yourDetails_applicationType') === 'joint') {
            $rules['yourDetails_partnerHealthConditions'] = 'required|boolean';

            $rules['yourDetails_healthConditionsThatApplyToYourPartner'] = [
                'array',
            ];

            $rules['yourDetails_healthConditionsThatApplyToYourPartner.*'] = [
                Rule::in([
                    'smokingMoreThan10CigarettesADay',
                    'cancer',
                    'ms',
                    'highBloodPressure',
                    'diabetes',
                    'peripheralVascularDisease',
                    'hepatitisC',
                    'parkinsons',
                    'motorNeuronDisease',
                    'heartAttack',
                    'stroke',
                    'miniStroke',
                    'hiv',
                    'dementia',
                    'kidneyFailure',
                    'organTransplant',
                    'cirrhosisOfLiver',
                    'heartValveReplacement',
                    'angina',
                    'earlyRetirement',
                ]),
            ];

            if ($this->request->get('yourDetails_partnerHealthConditions') == 1) {
                $rules['yourDetails_partnerHeightCm'] = 'numeric|between:1,299.9|required_without_all:yourDetails_partnerHeightFeet,yourDetails_partnerHeightInches';
                $rules['yourDetails_partnerHeightFeet'] = 'integer|between:1,9|required_without:yourDetails_partnerHeightCm';
                $rules['yourDetails_partnerHeightInches'] = 'numeric|between:0,12.9|required_without:yourDetails_partnerHeightCm';

                $rules['yourDetails_partnerWeightKgs'] = 'numeric|between:1,399.9|required_without:yourDetails_partnerWeightLbs';
                $rules['yourDetails_partnerWeightStones'] = 'integer|between:1,49|required_without_all:yourDetails_partnerWeightKgs,yourDetails_partnerWeightLbs';
                $rules['yourDetails_partnerWeightLbs'] = 'integer|between:0,13|required_without:yourDetails_partnerWeightKgs';
            }

            $rules['yourDetails_partnerMaritalStatus'] = [
                'required_if:loanDetails_equityReleasePlan,lifetimeMortgage',
                Rule::in([
                    'single',
                    'married',
                    'livingWithPartner',
                    'widowed',
                    'separated',
                    'divorced',
                    'civilPartnership',
                ])
            ];
        }

        if ($this->request->get('loanDetails_equityReleasePlan') === 'homeReversion') {
            $rules['yourDetails_partnerGender'] = [
                'required_if:yourDetails_applicationType,joint',
                Rule::in([
                    'male',
                    'female',
                ]),
            ];
        }

        if ($this->request->get('yourDetails_healthConditions') == 1) {
            $rules['yourDetails_yourHeightCm'] = 'numeric|between:1,299.9|required_without_all:yourDetails_yourHeightFeet,yourDetails_yourHeightInches';
            $rules['yourDetails_yourHeightFeet'] = 'integer|between:1,9|required_without:yourDetails_yourHeightCm';
            $rules['yourDetails_yourHeightInches'] = 'numeric|between:0,12.9|required_without:yourDetails_yourHeightCm';

            $rules['yourDetails_yourWeightKgs'] = 'numeric|between:1,399.9|required_without:yourDetails_yourWeightLbs';
            $rules['yourDetails_yourWeightStones'] = 'integer|between:1,49|required_without_all:yourDetails_yourWeightKgs,yourDetails_yourWeightLbs';
            $rules['yourDetails_yourWeightLbs'] = 'integer|between:0,13|required_without:yourDetails_yourWeightKgs';
        }

        return $rules;
    }

    /**
     * Configure rules for the 'credit status' section
     *
     * @return array
     */
    protected function configureCreditStatusRules(): array
    {
        $rules = [
            'creditStatus_arrearsOrDefaulted' => [
                'required',
                Rule::in([0, 1, 'unknown']),
            ],
            'creditStatus_debtManagementPlan' => [
                'required',
                Rule::in([0, 1, 'unknown']),
            ],
            'creditStatus_countyCourtJudgments' => [
                'required',
                Rule::in([0, 1, 'unknown']),
            ],
            'creditStatus_debtArrangementScheme' => [
                'required_if:yourDetails_country,scottishIsles',
                'required_if:yourDetails_country,mainlandScotland',
                Rule::in([0, 1, 'unknown']),
            ],
            'creditStatus_individualVoluntaryArrangementOrDebtReliefOrder' => [
                'required',
                Rule::in([0, 1, 'unknown']),
            ],
            'creditStatus_undischargedBankruptOrUnderSequestrationOrder' => [
                'required',
                Rule::in([0, 1, 'unknown']),
            ],
        ];

        return $rules;
    }

    /**
     * Configure rules for the 'property details' section
     *
     * @return array
     */
    protected function configurePropertyDetailsRules(): array
    {
        $propertyTenure = [];
        $country = $this->request->get('propertyDetails_country') ?? $this->request->get('yourDetails_country');

        if (in_array($country, [
            'england',
            'wales',
            'nIreland',
            'isleOfMan',
            'jersey',
            'guernsey',
            'channelIslands',
        ])) {
            $propertyTenure = ['freehold', 'leasehold'];
        }

        if (in_array($country, ['mainlandScotland', 'scottishIsles'])) {
            $propertyTenure = ['absolute', 'leasehold'];
        }

        $rules = [
            'propertyDetails_propertyType' => [
                'required',
                Rule::in([
                    'house',
                    'bungalow',
                    'maisonette',
                    'flat',
                ]),
            ],
            'propertyDetails_propertyExLocalAuthority' => 'required|boolean',
            'propertyDetails_propertyTenure' => [
                'required',
                Rule::in($propertyTenure),
            ],
            'propertyDetails_yearsRemainingOnLease' => 'required_if:propertyDetails_propertyTenure,leasehold|integer|between:1,999',
        ];

        if ($this->request->get('loanDetails_equityReleasePlan') === 'lifetimeMortgage') {
            $rules = array_merge($rules, [
                'propertyDetails_residentialProperty' => 'required|boolean',
                'propertyDetails_postcode' => 'required_if:propertyDetails_residentialProperty,0',
                'propertyDetails_addressLine1' => 'required_if:propertyDetails_residentialProperty,0',
                'propertyDetails_houseNumberOrName' => 'required_if:propertyDetails_residentialProperty,0',
                'propertyDetails_cityTown' => 'required_if:propertyDetails_residentialProperty,0',
                'propertyDetails_county' => 'required_if:propertyDetails_residentialProperty,0',
                'propertyDetails_country' => [
                    'required_if:propertyDetails_residentialProperty,0',
                    Rule::in([
                        'england',
                        'wales',
                        'mainlandScotland',
                        'nIreland',
                        'scottishIsles',
                        'isleOfMan',
                        'jersey',
                        'guernsey',
                        'channelIslands',
                    ]),
                ],
                'propertyDetails_addressSearchType' => 'in:postcode,manual',
                'propertyDetails_propertyValue' => 'required_if:propertyDetails_residentialProperty,0|numeric',
                'propertyDetails_propertyOwnership' => [
                    'required_if:propertyDetails_residentialProperty,0',
                    Rule::in([
                        'secondHolidayHome',
                        'buyToLet',
                    ]),
                ],

                'propertyDetails_haveYouAnyOutstandingMortgage' => 'required_if:propertyDetails_residentialProperty,0|boolean',
                'propertyDetails_haveYouAnyOutstandingMortgageAmount' => 'required_if:propertyDetails_haveYouAnyOutstandingMortgage,1|numeric',
            ]);
        }

        return $rules;
    }

    /**
     * Configure rules for the 'income status' section
     *
     * @return array
     */
    protected function configureIncomeStatusRules(): array
    {
        $rules = [
            'incomeStatus_employmentStatus' => 'required|array',
            'incomeStatus_employmentStatus.*' => Rule::in([
                'employed',
                'selfEmployed',
                'retired',
                'unemployed',
                'notWorking',
            ]),
            'incomeStatus_drawdownUnvestedPensionsAnnualIncome' => 'numeric',
            'incomeStatus_drawdownUnvestedPensionsFundSize' => 'numeric',
            'incomeStatus_investmentAnnualIncome' => 'numeric',
            'incomeStatus_investmentPensionsFundSize' => 'numeric',
            'incomeStatus_otherIncome' => 'numeric',
            'incomeStatus_comment' => 'max:600',

            // if the applicationType is joint
            'incomeStatus_partnerEmploymentStatus' => 'required_if:yourDetails_applicationType,joint|array',
            'incomeStatus_partnerEmploymentStatus.*' => Rule::in([
                'employed',
                'selfEmployed',
                'retired',
                'unemployed',
                'notWorking',
            ]),
            'incomeStatus_partnerDrawdownUnvestedPensionsAnnualIncome' => 'numeric',
            'incomeStatus_partnerDrawdownUnvestedPensionsFundSize' => 'numeric',
            'incomeStatus_partnerInvestmentAnnualIncome' => 'numeric',
            'incomeStatus_partnerInvestmentPensionsFundSize' => 'numeric',
            'incomeStatus_partnerOtherIncome' => 'numeric',
            'incomeStatus_partnerComment' => 'max:600',
            'incomeStatus_statePensionIncome' => 'required|numeric',
            'incomeStatus_occupationalPrivatePensionIncome' => 'required|numeric',
            'incomeStatus_partnerStatePensionIncome' => 'required_if:yourDetails_applicationType,joint|numeric',
        ];

        // Client
        $employmentStatus = !is_array($this->request->get('incomeStatus_employmentStatus', []))
            ? [$this->request->get('incomeStatus_employmentStatus')]
            : $this->request->get('incomeStatus_employmentStatus', []);

        if (in_array('employed', $employmentStatus, true)) {
            $rules['incomeStatus_employedIncome'] = 'required|numeric';
            $rules['incomeStatus_rentalIncome'] = 'numeric';
        }

        if (in_array('selfEmployed', $employmentStatus, true)) {
            $rules['incomeStatus_rentalIncome'] = 'numeric';
            $rules['incomeStatus_selfEmployedIncomeYear1'] = 'required|numeric';
            $rules['incomeStatus_selfEmployedIncomeYear2'] = 'required|numeric';
            $rules['incomeStatus_selfEmployedIncomeYear3'] = 'required|numeric';
        }

        if (in_array('unemployed', $employmentStatus) || in_array('notWorking', $employmentStatus)) {
            $rules['incomeStatus_otherIncome'] = 'required|numeric';
        }

        // Partner
        $pEmploymentStatus = !is_array($this->request->get('incomeStatus_partnerEmploymentStatus', []))
            ? [$this->request->get('incomeStatus_partnerEmploymentStatus')]
            : $this->request->get('incomeStatus_partnerEmploymentStatus', []);

        if (in_array('employed', $pEmploymentStatus, true)) {
            $rules['incomeStatus_partnerEmployedIncome'] = 'required|numeric';
            $rules['incomeStatus_partnerRentalIncome'] = 'numeric';
        }

        if (in_array('selfEmployed', $pEmploymentStatus, true)) {
            $rules['incomeStatus_partnerRentalIncome'] = 'numeric';
            $rules['incomeStatus_partnerSelfEmployedIncomeYear1'] = 'required|numeric';
            $rules['incomeStatus_partnerSelfEmployedIncomeYear2'] = 'required|numeric';
            $rules['incomeStatus_partnerSelfEmployedIncomeYear3'] = 'required|numeric';
        }

        if (in_array('unemployed', $pEmploymentStatus) || in_array('notWorking', $pEmploymentStatus)) {
            $rules['incomeStatus_partnerOtherIncome'] = 'required|numeric';
        }

        if (array_intersect(['retired', 'unemployed', 'notWorking', 'employed', 'selfEmployed'], $pEmploymentStatus)) {
            $rules['incomeStatus_partnerOccupationalPrivatePensionIncome'] = 'required|numeric';
        }

        if ($this->request->get('loanDetails_equityReleasePlan') === 'retirementMortgage') {
            $rules['incomeStatus_indicatePrimaryEarner'] = 'required_if:yourDetails_applicationType,joint|in:you,yourPartner';
        }

        return $rules;
    }

    /**
     * Custom rules
     * Here you can add custom validation rules
     *
     * @return array
     */
    abstract protected function configureCustomRules(): array;
}
