<?php

namespace EquityRelease\ClientCriteria\Providers;

use Illuminate\Support\ServiceProvider;

class CCFServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $source = realpath(__DIR__.'/../../config/ccf.php');

        $this->publishes([
            $source => config_path('ccf.php')
        ]);
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
