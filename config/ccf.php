<?php

return [
    'fields_settings' => [
        // Loan Details
        'loanDetails_equityRelease' => [
            'type' => 'array_of_strings',
            'label' => 'Purpose(s) for your release of equity',
            'options' => [
                'debtConsolidation' => 'Debt consolidation',
                'homeImprovements' => 'Home improvements',
                'repayMortgage' => 'Repay mortgage',
                'newSecondHome' => 'New/second home',
                'supplementRetirementIncome' => 'Supplement income',
                'newCar' => 'New Car',
                'gifting' => 'Gifting',
                'holiday' => 'Holiday',
                'inheritanceTax' => 'Inheritance tax',
                'carePlans' => 'Care planning',
                'emergencyFunds' => 'Emergency & lifestyle funds',
                'other' => 'Other',
            ],
        ],
        'loanDetails_whatWouldYouLikeToRelease' => [
            'type' => 'string',
            'label' => 'Type of release',
            'options' => [
                'cashLumpSum' => 'Lump Sum',
                'monthlyIncome' => 'Monthly Income',
            ],
        ],
        'loanDetails_howMatchRelease' => [
            'type' => 'string',
            'label' => 'What would you like to release?',
            'options' => [
                'maximum' => 'Maximum',
                'specificAmount' => 'Specific Amount',
            ],
        ],
        'loanDetails_equityReleasePlan' => [
            'type' => 'string',
            'label' => 'What type of equity release plan are you looking for?',
            'options' => [
                'lifetimeMortgage' => 'Lifetime Mortgage',
                'retirementMortgage' => 'Retirement Mortgage',
                'homeReversion' => 'Home Reversion',
                'rioMortgage' => 'RIO Mortgage',
            ],
        ],
        'loanDetails_mortgagePurpose' => [
            'type' => 'string',
            'label' => 'Mortgage purpose',
            'options' => [
                'housePurchase' => 'House Purchase',
                'remortgage' => 'Remortgage',
            ],
        ],
        'loanDetails_mortgageTermRequired' => [
            'type' => 'string',
            'label' => 'Mortgage term required?',
            'options' => [
                'fixedTerm' => 'Fixed Term',
                'lifetime' => 'Lifetime',
            ],
        ],
        'loanDetails_term' => [
            'type' => 'integer',
            'label' => 'Term in years',
        ],
        'loanDetails_howMatchReleaseSum' => [
            'type' => 'pounds',
            'label' => 'What specific amount do you require?',
            'label_callback' => function ($ccf) {
                if (isset($ccf['loanDetails_whatWouldYouLikeToRelease'])
                    && $ccf['loanDetails_whatWouldYouLikeToRelease'] == 'monthlyIncome')
                {
                    return 'What monthly income do you require?';
                }

                return 'What specific amount do you require?';
            },
        ],
        'loanDetails_overHowManyYearsIsIncomeRequired' => [
            'type' => 'array_of_strings',
            'label' => 'Over how many years is income Required?',
        ],
        'loanDetails_overHowManyYearsIsIncomeRequiredLifetime' => [
            'type' => 'boolean',
            'label' => 'Over how many years is income Required - Lifetime?',
        ],
        'loanDetails_sizeOfRelease' => [
            'type' => 'string',
            'label' => 'Confirm size of release',
        ],
        'loanDetails_lumpSum' => [
            'type' => 'pounds',
        ],
        'loanDetails_propertySell' => [
            'type' => 'integer',
        ],
        'loanDetails_repaymentPreference' => [
            'type' => 'string',
            'label' => 'Select repayment preference',
            'options' => [
                'capitalAndInterest' => 'Capital & Interest',
                'interestOnly' => 'Interest Only',
            ],
        ],

        // Your details
        'yourDetails_applicationType' => [
            'type' => 'string',
            'label' => 'What type of application are you applying for?',
            'options' => [
                'single' => 'Single application',
                'joint' => 'Joint application',
            ],
        ],
        'yourDetails_title' => [
            'type' => 'string',
            'label' => 'Your title',
            'options' => \App\Traits\Requests\Forms\TitleField::getTitles(),
        ],
        'yourDetails_firstName' => [
            'type' => 'string',
            'label' => 'Your first name',
        ],
        'yourDetails_surname' => [
            'type' => 'string',
            'label' => 'Your surname',
        ],
        'yourDetails_telephone' => [
            'type' => 'string',
            'label' => 'Your telephone number',
        ],
        'yourDetails_email' => [
            'type' => 'string',
            'label' => 'Your email address',
        ],
        'yourDetails_birthdayDay' => [
            'type' => 'integer',
            'label' => 'Your day of birth',
        ],
        'yourDetails_birthdayMonth' => [
            'type' => 'integer',
            'label' => 'Your month of birth',
        ],
        'yourDetails_birthdayYear' => [
            'type' => 'integer',
            'label' => 'Your year of birth',
        ],
        'yourDetails_birthdayPartnerDay' => [
            'type' => 'integer',
            'label' => 'Your joint applicants day of birth',
        ],
        'yourDetails_birthdayPartnerMonth' => [
            'type' => 'integer',
            'label' => 'Your joint applicants month of birth',
        ],
        'yourDetails_birthdayPartnerYear' => [
            'type' => 'integer',
            'label' => 'Your joint applicants year of birth',
        ],
        'yourDetails_gender' => [
            'type' => 'string',
            'label' => 'Your gender',
        ],
        'yourDetails_maritalStatus' => [
            'type' => 'string',
            'label' => 'Your marital status',
            'options' => [
                'single' => 'Single',
                'married' => 'Married',
                'livingWithPartner' => 'Living with partner',
                'widowed' => 'Widowed',
                'separated' => 'Separated',
                'divorced' => 'Divorced',
                'civilPartnership' => 'Civil Partnership',
            ],
        ],
        'yourDetails_healthConditions' => [
            'type' => 'boolean',
            'label' => 'Do you suffer from any health conditions?',
        ],
        'yourDetails_healthConditionsThatApplyToYou' => [
            'type' => 'array_of_strings',
            'label' => 'Please select the health condition(s) that apply to you',
            'options' => [
                'smokingMoreThan10CigarettesADay' => 'Smoking More Than 10 Cigarettes A Day',
                'cancer' => 'Cancer',
                'ms' => 'MS',
                'highBloodPressure' => 'High Blood Pressure',
                'diabetes' => 'Diabetes',
                'peripheralVascularDisease' => 'Peripheral Vascular Disease',
                'hepatitisC' => 'Hepatitis C',
                'parkinsons' => 'Parkinsons',
                'motorNeuronDisease' => 'Motor Neuron Disease',
                'heartAttack' => 'Heart Attack',
                'stroke' => 'Stroke',
                'miniStroke' => 'Mini Stroke',
                'hiv' => 'HIV',
                'dementia' => 'Dementia',
                'kidneyFailure' => 'Kidney Failure',
                'organTransplant' => 'Organ Transplant',
                'cirrhosisOfLiver' => 'Cirrhosis Of Liver',
                'heartValveReplacement' => 'Heart Valve Replacement',
                'angina' => 'Angina',
                'earlyRetirement' => 'Early Retirement',
            ],
        ],
        'yourDetails_yourHeightCm' => [
            'type' => 'string',
            'label' => 'Your height in cm',
        ],
        'yourDetails_yourHeightFeet' => [
            'type' => 'string',
            'label' => 'Your height in feet',
        ],
        'yourDetails_yourHeightInches' => [
            'type' => 'string',
            'label' => 'Your height in inches',
        ],
        'yourDetails_yourWeightKgs' => [
            'type' => 'string',
            'label' => 'Your weight in kilograms',
        ],
        'yourDetails_yourWeightStones' => [
            'type' => 'string',
            'label' => 'Your weight in stones',
        ],
        'yourDetails_yourWeightLbs' => [
            'type' => 'string',
            'label' => 'Your weight in lbs',
        ],
        'yourDetails_postcode' => [
            'type' => 'postcode',
            'label' => 'Your Postcode',
        ],
        'yourDetails_houseNumberOrName' => [
            'type' => 'string',
            'label' => 'House number or name',
        ],
        'yourDetails_addressLine1' => [
            'type' => 'string',
            'label' => 'Address line 1',
        ],
        'yourDetails_cityTown' => [
            'type' => 'string',
            'label' => 'City/Town',
        ],
        'yourDetails_county' => [
            'type' => 'string',
            'label' => '',
        ],
        'yourDetails_country' => [
            'type' => 'string',
            'label' => 'Country',
        ],
        'yourDetails_doYouOwnThisProperty' => [
            'type' => 'boolean',
            'label' => 'Do you own',
            'label_callback' => function ($ccf) {
                if (isset($ccf['yourDetails_houseNumberOrName'])
                    && isset($ccf['yourDetails_addressLine1'])) {
                    return 'Do you own '
                        . $ccf['yourDetails_houseNumberOrName']
                        . ' '
                        . $ccf['yourDetails_addressLine1'];
                }

                return 'Are you owner';
            },
        ],
        'yourDetails_propertyValue' => [
            'type' => 'pounds',
            'label' => 'Property value',
        ],
        'yourDetails_outstandingMortgageOnThisProperty' => [
            'type' => 'boolean',
            'label' => 'Have you any outstanding mortgage on this property?',
        ],
        'yourDetails_mortgageBalanceOutstanding' => [
            'type' => 'pounds',
            'label' => 'What is the outstanding mortgage amount?',
        ],
        'propertyDetails_propertyType' => [
            'type' => 'string',
            'label' => 'What type of building?',
            'options' => [
                'house' => 'House',
                'bungalow' => 'Bungalow',
                'maisonette' => 'Maisonette',
                'flat' => 'Flat/Apartment',
            ],
        ],
        'propertyDetails_propertyExLocalAuthority' => [
            'type' => 'boolean',
            'label' => 'Is the property ex-local authority?',
        ],
        'propertyDetails_propertyTenure' => [
            'type' => 'string',
            'label' => 'What is the tenure?',
        ],
        'propertyDetails_yearsRemainingOnLease' => [
            'type' => 'integer',
            'label' => 'Years remaining on lease',
        ],
        'yourDetails_partnerGender' => [
            'type' => 'string',
            'label' => 'Your joint applicants gender',
        ],
        'yourDetails_partnerMaritalStatus' => [
            'type' => 'string',
            'label' => 'Partner marital status',
            'options' => [
                'single' => 'Single',
                'married' => 'Married',
                'livingWithPartner' => 'Living with partner',
                'widowed' => 'Widowed',
                'separated' => 'Separated',
                'divorced' => 'Divorced',
                'civilPartnership' => 'Civil Partnership',
            ],
        ],
        'yourDetails_partnerHealthConditions' => [
            'type' => 'boolean',
            'label' => 'Does your joint applicant suffer from any health conditions?',
        ],
        'yourDetails_healthConditionsThatApplyToYourPartner' => [
            'type' => 'array_of_strings',
            'label' => 'Please select the health condition(s) that apply to your partner',
            'options' => [
                'smokingMoreThan10CigarettesADay' => 'Smoking More Than 10 Cigarettes A Day',
                'cancer' => 'Cancer',
                'ms' => 'MS',
                'highBloodPressure' => 'High Blood Pressure',
                'diabetes' => 'Diabetes',
                'peripheralVascularDisease' => 'Peripheral Vascular Disease',
                'hepatitisC' => 'Hepatitis C',
                'parkinsons' => 'Parkinsons',
                'motorNeuronDisease' => 'Motor Neuron Disease',
                'heartAttack' => 'Heart Attack',
                'stroke' => 'Stroke',
                'miniStroke' => 'Mini Stroke',
                'hiv' => 'HIV',
                'dementia' => 'Dementia',
                'kidneyFailure' => 'Kidney Failure',
                'organTransplant' => 'Organ Transplant',
                'cirrhosisOfLiver' => 'Cirrhosis Of Liver',
                'heartValveReplacement' => 'Heart Valve Replacement',
                'angina' => 'Angina',
                'earlyRetirement' => 'Early Retirement',
            ],
        ],
        'yourDetails_partnerHeightCm' => [
            'type' => 'string',
            'label' => 'Partner height in cm',
        ],
        'yourDetails_partnerHeightFeet' => [
            'type' => 'string',
            'label' => 'Partner height in feet',
        ],
        'yourDetails_partnerHeightInches' => [
            'type' => 'string',
            'label' => 'Partner height in inches',
        ],
        'yourDetails_partnerWeightKgs' => [
            'type' => 'string',
            'label' => 'Partner weight in kilograms',
        ],
        'yourDetails_partnerWeightStones' => [
            'type' => 'string',
            'label' => 'Partner weight in stones',
        ],
        'yourDetails_partnerWeightLbs' => [
            'type' => 'string',
            'label' => 'Partner weight in lbs',
        ],

        // Income status
        'incomeStatus_employmentStatus' => [
            'type' => 'array_of_strings',
            'label' => 'Your: Employment status',
        ],
        'incomeStatus_employedIncome' => [
            'type' => 'pounds',
            'label' => 'Your: Employed Income',
        ],
        'incomeStatus_selfEmployedIncomeYear1' => [
            'type' => 'pounds',
            'label' => 'Your: Year 1',
        ],
        'incomeStatus_selfEmployedIncomeYear2' => [
            'type' => 'pounds',
            'label' => 'Your: Year 2',
        ],
        'incomeStatus_selfEmployedIncomeYear3' => [
            'type' => 'pounds',
            'label' => 'Your: Year 3',
        ],
        'incomeStatus_statePensionIncome' => [
            'type' => 'pounds',
            'label' => 'Your: State Pension Income',
        ],
        'incomeStatus_occupationalPrivatePensionIncome' => [
            'type' => 'pounds',
            'label' => 'Your: Occupational/Private Pension Income',
        ],
        'incomeStatus_rentalIncome' => [
            'type' => 'pounds',
            'label' => 'Your: Rental Income',
        ],
        'incomeStatus_drawdownUnvestedPensionsAnnualIncome' => [
            'type' => 'pounds',
            'label' => 'Your: Annual Income',
        ],
        'incomeStatus_drawdownUnvestedPensionsFundSize' => [
            'type' => 'pounds',
            'label' => 'Your: Fund Size',
        ],
        'incomeStatus_investmentAnnualIncome' => [
            'type' => 'pounds',
            'label' => 'Your: Annual Income',
        ],
        'incomeStatus_investmentPensionsFundSize' => [
            'type' => 'pounds',
            'label' => 'Your: Fund Size',
        ],
        'incomeStatus_otherIncome' => [
            'type' => 'pounds',
            'label' => 'Your: Other Income',
        ],
        'incomeStatus_comment' => [
            'type' => 'string',
            'label' => 'Your: Comments here',
        ],
        'incomeStatus_partnerEmploymentStatus' => [
            'type' => 'array_of_strings',
            'label' => 'Your Partner: Employment status',
        ],
        'incomeStatus_partnerEmployedIncome' => [
            'type' => 'pounds',
            'label' => 'Your Partner: Employed Income',
        ],
        'incomeStatus_partnerSelfEmployedIncomeYear1' => [
            'type' => 'pounds',
            'label' => 'Your Partner: Year 1',
        ],
        'incomeStatus_partnerSelfEmployedIncomeYear2' => [
            'type' => 'pounds',
            'label' => 'Your Partner: Year 2',
        ],
        'incomeStatus_partnerSelfEmployedIncomeYear3' => [
            'type' => 'pounds',
            'label' => 'Your Partner: Year 3',
        ],
        'incomeStatus_partnerStatePensionIncome' => [
            'type' => 'pounds',
            'label' => 'Your Partner: State Pension Income',
        ],
        'incomeStatus_partnerOccupationalPrivatePensionIncome' => [
            'type' => 'pounds',
            'label' => 'Your Partner: Occupational/Private Pension Income',
        ],
        'incomeStatus_partnerRentalIncome' => [
            'type' => 'pounds',
            'label' => 'Your Partner: Rental Income',
        ],
        'incomeStatus_partnerDrawdownUnvestedPensionsAnnualIncome' => [
            'type' => 'pounds',
            'label' => 'Your Partner: Annual Income',
        ],
        'incomeStatus_partnerDrawdownUnvestedPensionsFundSize' => [
            'type' => 'pounds',
            'label' => 'Your Partner: Fund Size',
        ],
        'incomeStatus_partnerInvestmentAnnualIncome' => [
            'type' => 'pounds',
            'label' => 'Your Partner: Annual Income',
        ],
        'incomeStatus_partnerInvestmentPensionsFundSize' => [
            'type' => 'pounds',
            'label' => 'Your Partner: Fund Size',
        ],
        'incomeStatus_partnerOtherIncome' => [
            'type' => 'pounds',
            'label' => 'Your Partner: Other Income',
        ],
        'incomeStatus_partnerComment' => [
            'type' => 'string',
            'label' => 'Your Partner: Comments here',
        ],
        'incomeStatus_indicatePrimaryEarner' => [
            'type' => 'string',
            'label' => 'Indicate primary earner',
        ],

        // Credit Status
        'creditStatus_arrearsOrDefaulted' => [
            'type' => 'boolean',
            'label' => 'Have you been in arrears or defaulted in the last 6 years?',
        ],
        'creditStatus_debtManagementPlan' => [
            'type' => 'boolean',
            'label' => 'Are you currently in a Debt Management Plan?',
        ],
        'creditStatus_countyCourtJudgments' => [
            'type' => 'boolean',
            'label' => 'Have you any outstanding County Court Judgments (CCJs)?',
        ],
        'creditStatus_debtArrangementScheme' => [
            'type' => 'boolean',
            'label' => 'Are you currently in a Debt Arrangement Scheme (Scotland)?',
        ],
        'creditStatus_individualVoluntaryArrangementOrDebtReliefOrder' => [
            'type' => 'boolean',
            'label' => 'Are you currently in an Individual Voluntary Arrangement (IVA), or Debt Relief Order?',
        ],
        'creditStatus_undischargedBankruptOrUnderSequestrationOrder' => [
            'type' => 'boolean',
            'label' => 'Are you an undischarged bankrupt, or under sequestration order?',
        ],

        // Property details
        'propertyDetails_residentialProperty' => [
            'type' => 'boolean',
            'label' => 'Is the property you wish to release equity from your residential property?',
        ],
        'propertyDetails_propertyValue' => [
            'type' => 'pounds',
            'label' => 'Property value',
            'label_callback' => function ($ccf) {
                if (isset($ccf['propertyDetails_residentialProperty'])
                    && $ccf['propertyDetails_residentialProperty'] == 0)
                {
                    return 'Property value of the second property';
                }

                return 'Property value';
            },
        ],
        'propertyDetails_propertyOwnership' => [
            'type' => 'string',
            'label' => 'How do you own?',
            'label_callback' => function ($ccf) {
                if (isset($ccf['propertyDetails_residentialProperty'])
                    && $ccf['propertyDetails_residentialProperty'] == 0)
                {
                    return 'How do you own the second property?';
                }

                return 'How do you own?';
            },
            'options' => [
                'secondHolidayHome' => 'Second Or Holiday Home',
                'buyToLet' => 'Buy To Let property',
            ],
        ],
        'propertyDetails_postcode' => [
            'type' => 'postcode',
            'label' => 'What is the postcode for the property you want to release equity from?',
        ],
        'propertyDetails_houseNumberOrName' => [
            'type' => 'string',
            'label' => 'House number or name',
        ],
        'propertyDetails_addressLine1' => [
            'type' => 'string',
            'label' => 'Address line 1',
        ],
        'propertyDetails_cityTown' => [
            'type' => 'string',
            'label' => 'City/Town',
        ],
        'propertyDetails_county' => [
            'type' => 'string',
            'label' => 'County',
        ],
        'propertyDetails_country' => [
            'type' => 'string',
            'label' => 'Country',
        ],
        'propertyDetails_haveYouAnyOutstandingMortgage' => [
            'type' => 'boolean',
            'label' => 'Have you any outstanding mortgage',
            'label_callback' => function ($ccf) {
                if (isset($ccf['propertyDetails_residentialProperty'])
                    && $ccf['propertyDetails_residentialProperty'] == 0)
                {
                    return 'Have you any outstanding mortgage of the second property?';
                }

                return 'Have you any outstanding mortgage';
            },
        ],
        'propertyDetails_haveYouAnyOutstandingMortgageAmount' => [
            'type' => 'pounds',
            'label' => 'What is the outstanding mortgage amount?',
            'label_callback' => function ($ccf) {
                if (isset($ccf['propertyDetails_residentialProperty'])
                    && $ccf['propertyDetails_residentialProperty'] == 0)
                {
                    return 'What is the outstanding mortgage amount of the second property?';
                }

                return 'What is the outstanding mortgage amount?';
            },
        ],
    ],
];
